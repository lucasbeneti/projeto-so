import socket
import sys

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

client_ip = "169.254.87.178"
port = 9090

server_address = (client_ip, port)
print('starting up on {} port {}'.format(*server_address))

sock.bind(server_address)
sock.listen(1)

while True:
    print('waiting for a connection')
    connection, client_address = sock.accept()

    try:
        print('connection from', client_address)
        while True:
            data = connection.recv(100)
            print('received {!r}'.format(data))
            break
            # if data:
            #     print('sending data back to the client')
            #     connection.sendall(data)
            # else:
            #     print('no data from', client_address)
            #     break
    finally:
        connection.close()

