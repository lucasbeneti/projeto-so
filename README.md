# Compilar projeto local (testado no Windows somente):
## Passos para fazer depois de ter exportado o projeto do compiler online da mbed:
* apagar todas as pastas relacionadas a mbed
* mbed new . (ponto para que seja criado um projeto no diretório atual)
* mbed sync
* mbed deploy
* mbed compile -m K64F -t GCC_ARM -f
* 

<b>TODO (edit: 07/12/19):</b>
* [X] Criar um TCPServer para ter um jeito de ouvir a porta que estou mandando. <b>Já feito, só precisava fazer com que esse servidor python aqui ouvisse uma determinada porta para onde o socket estaria mandando os dados</b>
O que tá acontecendo hoje é que eu tenho a interface ethernet, tenho um socker criado
e até mando um dado por esse socket para uma porta, teóricamente. Porém é preciso um
server que fique escutando determinada porta pra que eu possa pegar a informação que está
sendo enviada para ela. [Aqui](https://os.mbed.com/questions/1696/Why-is-Socketreceive-blocking-when-I-set/) tem um cara com um problema parecido onde ele tem implementado
um server de socket.
* [ ] Desenhar certinho o que precisa ser mandado e o que precisa ser recebido
* [ ] Determinar funcionamento e funções do server de socket
* [ ] Fazer e organizar as threads que foram pedidas
* [ ] (Opcional) Refatorar o código

<b>Funcionamento do servidor:</b>
O servidor tem um IP dele e o cliente (a placa tem outro, só dela, que tá no range de IPs que estão conectados à minha rede). Tendo esses dois IPs, eu conecto ethernet da Freescale com o IP do cliente (a própria placa no caso) e para mandar dados, faço o socket mandar no IP do server numa porta específica, onde o server estará ouvindo constantemente o que estiver rolando naquela porta dele.

<b>Erro cometido:</b>
* estava conectando a placa e mandando dado no mesmo IP, desse jeito, mesmo com o servidor escutando na mesma porta, não tinha como pegar nada porque estaria concorrendo com a placa